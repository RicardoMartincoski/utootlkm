# SPDX-License-Identifier: GPL-2.0-or-later
# copied from Buildroot 2021.02
# and then updated:
# - to not point to Buildroot mailing list or docker hub
# - to allow multiple layers on docker to improve regeneration of image
# - to use sed to select snapshot mirror instead of copying a file
# - to use an image with Python >= 3.6 in order to run kunit
# - to install cmake > 3.15 in order to speed up build

# This Dockerfile generates the docker image that gets used by Gitlab CI
# To build it (YYYYMMDD.HHMM is the current date and time in UTC):
#   sudo docker build -t ricardomartincoski/utootlkm:YYYYMMDD.HHMM support/docker
#   sudo docker push ricardomartincoski/utootlkm:YYYYMMDD.HHMM

FROM debian:buster-20210511

LABEL maintainer="Ricardo Martincoski <ricardo.martincoski@gmail.com>" \
      description="Container with everything needed to run utootlkm"

# Setup environment
ENV DEBIAN_FRONTEND noninteractive

# This repository can be a bit slow at times. Don't panic...
RUN sed -e 's,^deb.*,,g' -e 's,^# deb,deb [check-valid-until=no],g' -i /etc/apt/sources.list

# The container has no package lists, so need to update first
RUN dpkg --add-architecture i386 && \
    apt-get update -y
RUN apt-get install -y --no-install-recommends \
        bc \
        build-essential \
        bzr \
        ca-certificates \
        cmake \
        cpio \
        cvs \
        file \
        g++-multilib \
        git \
        libc6:i386 \
        libncurses5-dev \
        locales \
        mercurial \
        python3 \
        python3-flake8 \
        python3-nose2 \
        python3-pexpect \
        qemu-system-arm \
        qemu-system-x86 \
        rsync \
        subversion \
        unzip \
        wget \
        && echo done

RUN apt-get -y autoremove && \
    apt-get -y clean

# install cmake > 3.15 in order to speed up build
ARG cmake_version=3.20.2
RUN apt-get purge -y cmake && \
    wget --progress=dot:giga https://github.com/Kitware/CMake/releases/download/v${cmake_version}/cmake-${cmake_version}-linux-x86_64.sh && \
    chmod +x cmake-${cmake_version}-linux-x86_64.sh && \
    ./cmake-${cmake_version}-linux-x86_64.sh --skip-license && \
    rm -f cmake-${cmake_version}-linux-x86_64.sh

# To be able to generate a toolchain with locales, enable one UTF-8 locale
RUN sed -i 's/# \(en_US.UTF-8\)/\1/' /etc/locale.gen && \
    /usr/sbin/locale-gen

RUN useradd -ms /bin/bash br-user && \
    chown -R br-user:br-user /home/br-user

USER br-user
WORKDIR /home/br-user
ENV HOME /home/br-user
ENV LC_ALL en_US.UTF-8
