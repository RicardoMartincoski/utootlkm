################################################################################
#
# realtek-r8152-linux
#
################################################################################

REALTEK_R8152_LINUX_VERSION = v2.14
REALTEK_R8152_LINUX_SITE = https://github.com/wget/realtek-r8152-linux.git
REALTEK_R8152_LINUX_SITE_METHOD = git
# backup: https://gitlab.com/RicardoMartincoski/realtek-r8152-linux
REALTEK_R8152_LINUX_LICENSE = GPL-2.0
# no license file
REALTEK_R8152_LINUX_LICENSE_FILES = r8152.c
REALTEK_R8152_LINUX_MODULE_MAKE_OPTS = KERNELRELEASE=$(LINUX_VERSION_PROBED)

ifeq ($(BR2_PACKAGE_REALTEK_R8152_LINUX_TEST),y)

define REALTEK_R8152_LINUX_ADD_TEST
	cp -f $(REALTEK_R8152_LINUX_PKGDIR)/r8152-test.c $(@D)/r8152-test.c
	echo 'obj-m += r8152-test.o' >> $(@D)/Makefile
endef
REALTEK_R8152_LINUX_POST_EXTRACT_HOOKS += REALTEK_R8152_LINUX_ADD_TEST

define REALTEK_R8152_LINUX_LINUX_CONFIG_FIXUPS
	$(call KCONFIG_ENABLE_OPT,CONFIG_DEBUG_FS)
	$(call KCONFIG_ENABLE_OPT,CONFIG_KUNIT)
	$(call KCONFIG_ENABLE_OPT,CONFIG_KUNIT_DEBUGFS)
	$(call KCONFIG_ENABLE_OPT,CONFIG_USB_XHCI_HCD)
	$(call KCONFIG_DISABLE_OPT,CONFIG_PM)
	$(call KCONFIG_DISABLE_OPT,CONFIG_SUSPEND)
endef
endif # BR2_PACKAGE_REALTEK_R8152_LINUX_TEST

$(eval $(kernel-module))
$(eval $(generic-package))
