# utootlkm

------

## License
utootlkm
Copyright (C) 2021  Ricardo Martincoski

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

------

There are some files in the tree copied from other projects.
They have the license stated in the header of the file. For instance:
```
SPDX-License-Identifier: GPL-2.0-or-later
copied from Buildroot 2021.02
```

They are all, to the best of my knowledge (I am not a layer), licensed using
compatible license.
In the case you disagree please contact me and I will be happy to fix the issue.

------

## How to run the tests

Using a computer with `Ubuntu 20.04.2.0` and Internet access, execute following
steps that would take around 1 to 2 hours, depending on your computer and your
Internet speed, and around 6 GB of hard drive.

1) Install `docker` and add user to group:
```
$ sudo apt install docker.io
$ sudo groupadd docker
$ sudo usermod -aG docker $USER # NOTE: logout/login after this
$ docker run hello-world
```

2) Install `git` and `make`:
```
$ sudo apt install git make
```

3) Download the repo and run the tests
```
$ git clone --depth 1 --recurse-submodules -b v1.0 \
  https://gitlab.com/RicardoMartincoski/utootlkm.git
$ make -C utootlkm/
```
